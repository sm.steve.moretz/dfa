import React, {useEffect, useRef, useState} from 'react';
import './App.css';
import MenuItem from "@material-ui/core/MenuItem";
import Select from "@material-ui/core/Select";
import AddBoxIcon from '@material-ui/icons/AddBox';
import CloseIcon from '@material-ui/icons/Close';
import BackspaceIcon from '@material-ui/icons/Backspace';
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Box from "@material-ui/core/Box";
import Snackbar from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import YouTubeIcon from '@material-ui/icons/YouTube';
import GitHubIcon from '@material-ui/icons/GitHub';

const useStyles = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #2196F3 30%, #21CBF3 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(71,83,213,0.75)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
    buttonRemove:{
        background: 'linear-gradient(45deg, purple 30%, red 90%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px rgba(212,0,82,0.58)',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
    buttonSubmit:{
        background: 'linear-gradient(45deg, green 30%, yellow 100%)',
        border: 0,
        borderRadius: 3,
        boxShadow: '0 3px 5px 2px green',
        color: 'white',
        height: 48,
        padding: '0 30px',
    },
    table :{
        '& th , & td': {
            border : '1px solid gray'
        }
    }
});

let firstArray = [];
let secondArray = [];
let tempPrintArray = [];
let reachableNodes = [];
function App() {
    const classes = useStyles();
    const [selectedNodes, setSelectedNodes] = useState({});
    const [finalStates, setFinalStates] = useState({});
    const [equivalencySteps, setEquivalencySteps] = useState([]);


    const handleChange = (col,item,event) => {
        let obj;
        switch (col) {
            case 0:
                obj = {zero : event.target.value};
                break;
            case 1:
                obj = {one : event.target.value};
                break;
        }
        setEquivalencySteps([]);
        setReachableNodesToWorkWith([]);
        setSelectedNodes({...selectedNodes,[item]:{...selectedNodes[item],...obj}})
    };

    const [listOfNodes, setListOfNodes] = useState([]);
    const getNextNodeAsciiCode = ()=>{return listOfNodes.length > 0 ? listOfNodes[listOfNodes.length-1] + 1 : 65;};

    const onAddNewNode = () => {
        setEquivalencySteps([]);
        setReachableNodesToWorkWith([]);
        const code = getNextNodeAsciiCode();
        setSelectedNodes({...selectedNodes,[code]:{zero : code,one:code}});
        setListOfNodes([...listOfNodes,code]);
    };
    const onRemoveLastNode = () => {
        setEquivalencySteps([]);
        setReachableNodesToWorkWith([]);
        const lastItem = listOfNodes[listOfNodes.length-1];
        let newSelectedNodes = {};
        for(let [key,value] of Object.entries(selectedNodes)){
            if(key !== lastItem){
                if(value.one === lastItem){
                    value = {...value,one : key}
                }
                if(value.zero === lastItem){
                    value = {...value,zero : key}
                }
                newSelectedNodes = {...newSelectedNodes,[key]:value};
            }
        }
        setFinalStates({...finalStates,[lastItem] : false});
        setSelectedNodes(newSelectedNodes);
        setListOfNodes(listOfNodes.splice(0,listOfNodes.length-1))
    };

    const printArrayContainingAsciis = (a) => {
        if(!a)return '';
        const arr = a.map((item)=>{
            if(typeof item === 'object' && item.length) {
                return item.map((i)=>{
                    return String.fromCharCode(i);
                });
            }else{
                return String.fromCharCode(item);
            }
        });
        return JSON.stringify(arr).replace(/"/g,'').replace(/\[/g,'{').replace(/\]/g,'}');
    };

    const existsInTheSameObjectInFirstArray = (item1,item2) => {
        for (let element of firstArray) {
            const a = element.indexOf(item1);
            const b = element.indexOf(item2);
            if(a > -1 || b > -1){return a > -1 && b > -1;}
        }
    };

    const getArrayContaining = (item) => {
        for (let element of secondArray) {
            const a = element.indexOf(item);
            if(a > -1)return element;
        }
    };

    const arraysAreEqual = (a,b) => {
        if(a === b) return true;
        if(a === null || b === null) return false;
        if(a.length !== b.length) return false;
        for(let i = 0; i < a.length; ++i){
            if(typeof a[i] === 'object' && a[i].length){
                if(!arraysAreEqual(a[i],b[i])){return false}
            }else{
                if(a[i] !== b[i]) return false;
            }
        }
        return true;
    };

    const checkNthEquivalency = (itemF1,itemS1) => {
        // console.log('itemF1 : ' + itemF1);
        // console.log('itemS1 : ' + itemS1);
        const zeroF1 = selectedNodes[itemF1].zero;
        const zeroS1 = selectedNodes[itemS1].zero;
        const zerosExistInFirstArray = existsInTheSameObjectInFirstArray(zeroF1,zeroS1);
        const cond1 = zeroF1 === zeroS1 || zerosExistInFirstArray;
        const oneF1 = selectedNodes[itemF1].one;
        const oneS1 = selectedNodes[itemS1].one;
        const onesExistInFirstArray = existsInTheSameObjectInFirstArray(oneF1,oneS1);
        const cond2 = oneF1 === oneS1 || onesExistInFirstArray;
        return cond1 && cond2;
    };

    const findUnreachableNodes = (item = listOfNodes[0]) => {
        if(item === listOfNodes[0]){reachableNodes = [item]}
        const zero = selectedNodes[item].zero;
        const one = selectedNodes[item].one;
        if(reachableNodes.indexOf(zero) === -1){
            reachableNodes = [...reachableNodes,zero];
            findUnreachableNodes(zero);
        }
        if(reachableNodes.indexOf(one) === -1){
            reachableNodes = [...reachableNodes,one];
            findUnreachableNodes(one);
        }
    };

    const [reachableNodesToWorkWith, setReachableNodesToWorkWith] = useState([]);

    const startProcess = () => {
        let hasFinalStates = false;
        for(let [value] of Object.entries(finalStates)){
            if(value){
                hasFinalStates = true;
                break;
            }
        }
        if(hasFinalStates){
            findUnreachableNodes();
            setReachableNodesToWorkWith([...reachableNodes]);
            reachableNodes = [];
        }else{
            setOpen(true);
        }
    };

    useEffect(() => {
        if(reachableNodesToWorkWith.length > 0){
            computeEquivalency();
            secondArray = secondArray.filter((item)=>item.length > 0);
        }
    }, [reachableNodesToWorkWith]);


    const computeEquivalency = (zeroEquivalency = true) => {
        if(zeroEquivalency){//Zero Equivalency First
            let obj1 = [];
            let obj2 = [];
            reachableNodesToWorkWith.forEach((item)=>{
                const finalState = finalStates[item];
                if(finalState){
                    obj1 = [...obj1,item];
                }else{
                    obj2 = [...obj2,item]
                }
            });
            firstArray = [obj2,obj1];
            secondArray = [];
            tempPrintArray = [printArrayContainingAsciis(firstArray.filter((item)=>item.length > 0))];
            computeEquivalency(false);
        }else{
            firstArray.forEach((item)=>{
                if(item.length > 1){
                    item.forEach((itemInArray)=>{
                        let foundEquivalencyInSecondArray = false;
                        let index = 0;
                        for (let itemS of secondArray) {
                            foundEquivalencyInSecondArray = checkNthEquivalency(itemInArray,itemS[0]);
                            if(foundEquivalencyInSecondArray)break;
                            index++;
                        }
                        if(foundEquivalencyInSecondArray){
                            secondArray[index] = [...secondArray[index],itemInArray]
                        }else{
                            secondArray = [...secondArray,[itemInArray]];
                        }
                    });
                }else{
                    secondArray = [...secondArray,item];
                }
            });
            const weAreDone = arraysAreEqual(secondArray, firstArray);
            if(weAreDone){
                setEquivalencySteps([...tempPrintArray]);
                tempPrintArray = [];
            }else{
                tempPrintArray = [...tempPrintArray,printArrayContainingAsciis(secondArray.filter((item)=>item.length > 0))];
                firstArray = secondArray;
                secondArray = [];
                computeEquivalency(false)
            }
        }
    };

    const checkIfIsFinalState = (arr) => {
        for(let obj of arr){
            console.log(obj);
            if(!!finalStates[obj])return true;
        }
        return false;
    };

    const [open, setOpen] = React.useState(false);

    const handleClose = (event, reason) => {
        setOpen(false);
    };

    const lastTableRef = useRef();
    const secondLastTableRef = useRef();

    useEffect(() => {
        if(secondLastTableRef.current){
            secondLastTableRef.current.scrollIntoView({behavior : 'smooth'})
        }else if(lastTableRef.current){
            // lastTableRef.current.scrollIntoView({behavior : 'smooth'})
        }
    }, [equivalencySteps,reachableNodesToWorkWith]);


    return (
        <div className="App" style={{backgroundColor : '#4d4d4d',paddingTop : 50,paddingBottom : 80}}>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                open={open}
                autoHideDuration={6000}
                onClose={handleClose}
                message="لطفا با کلیک روی حروف الفبا (در ستون نود) خانه های نهایی را تعیین کنید."
                action={
                    <React.Fragment>
                        <IconButton size="small" aria-label="close" color="inherit" onClick={handleClose}>
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
            <Box display="flex" flexDirection="column">
                <Typography style={{color : 'white',fontSize : 42}}>Dfa Reduction</Typography>
                <Box style={{width : '80%',backgroundColor : 'rgba(255,255,255,0.5)',height : 2,alignSelf : 'center'}}/>
                <Box display="flex" flexDirection="column" alignItems='center' justifyContent='center'>
                    <Button onClick={()=>{
                        window.location.href = 'https://www.youtube.com/watch?v=0XaGAkY09Wc'
                    }} style={{marginBottom : 2}}>
                        <Box display="flex" flexDirection="row" alignItems="center">
                            <Typography style={{color : 'white',fontSize : 15}}>الگوریتم بر اساس توضیحات ویدو های کانال</Typography>
                            <YouTubeIcon style={{color : 'red',marginLeft : 5,fontSize : 35}}/>
                        </Box>
                    </Button>
                    <Button onClick={()=>{
                        window.location.href = 'https://gitlab.com/sm.steve.moretz/dfa'
                    }} style={{marginBottom : 5}}>
                        <Box display="flex" flexDirection="row" alignItems="center" style={{marginLeft : 58}}>
                            <Typography style={{color : 'white',fontSize : 15}}>مشاهده سورس کد ها در گیت لب</Typography>
                            <GitHubIcon style={{color : 'white',marginLeft : 7,fontSize : 28}}/>
                        </Box>
                    </Button>
                </Box>
                <Box style={{width : '80%',backgroundColor : 'rgba(255,255,255,0.5)',height : 2,alignSelf : 'center',marginBottom : 20}}/>
                <Box display="flex" flexDirection="column" alignItems="center" alignSelf='center'>
                    <Box display="flex" flexDirection="row" alignItems="center">
                        <Box display='flex' style={{marginRight : 8}}>
                            <Button
                                variant="contained"
                                color="primary"
                                startIcon={<BackspaceIcon/>}
                                className={classes.buttonRemove}
                                onClick={onRemoveLastNode}
                            >
                                پاک کردن نود
                            </Button>
                        </Box>
                        <Box display='flex'>
                            <Button
                                variant="contained"
                                color="primary"
                                startIcon={<AddBoxIcon/>}
                                className={classes.root}
                                onClick={onAddNewNode}
                            >
                                اضافه کردن نود
                            </Button>
                        </Box>
                    </Box>
                    <TableContainer component={Paper} style={{marginTop : 5,minWidth : 300}}>
                        <Table aria-label="simple table" className={classes.table}>
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">نود</TableCell>
                                    <TableCell align="center">۰</TableCell>
                                    <TableCell align="center">۱</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    listOfNodes.map((item)=>{
                                        return <TableRow key={item.toString()}>
                                            <TableCell align="center">
                                                <Button onClick={()=>{
                                                    setEquivalencySteps([]);
                                                    setReachableNodesToWorkWith([]);
                                                    setFinalStates({...finalStates,[item]:!finalStates[item]})
                                                }} style={{borderRadius : 35,width : 70,height : 70,border : finalStates[item] ? '1px solid purple':undefined}}>
                                                    <Box style={{width : 50,height :50,borderRadius : 25,border : finalStates[item] ? '1px solid purple':undefined}} display='flex' alignItems='center' justifyContent='center'>
                                                        <Typography>{String.fromCharCode(item)}</Typography>
                                                    </Box>
                                                </Button>
                                            </TableCell>
                                            <TableCell align="center">
                                                <Select
                                                    labelId="demo-simple-select-helper-label"
                                                    id="demo-simple-select-helper"
                                                    value={selectedNodes[item].zero}
                                                    onChange={handleChange.bind(this,0,item)}
                                                >
                                                    {
                                                        listOfNodes.map((i)=>{
                                                            return (
                                                                <MenuItem key={i.toString()} value={i}>{String.fromCharCode(i)}</MenuItem>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </TableCell>
                                            <TableCell align="center">
                                                <Select
                                                    labelId="demo-simple-select-helper-label"
                                                    id="demo-simple-select-helper"
                                                    value={selectedNodes[item].one}
                                                    onChange={handleChange.bind(this,1,item)}
                                                >
                                                    {
                                                        listOfNodes.map((i)=>{
                                                            return (
                                                                <MenuItem key={i.toString()} value={i}>{String.fromCharCode(i)}</MenuItem>
                                                            )
                                                        })
                                                    }
                                                </Select>
                                            </TableCell>
                                        </TableRow>
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <Button
                        variant="contained"
                        color="primary"
                        className={classes.buttonSubmit}
                        onClick={startProcess}
                        style={{marginTop : 10,width : '100%'}}
                    >
                        محاسبه
                    </Button>
                    {
                        (0<reachableNodesToWorkWith.length) && (reachableNodesToWorkWith.length < listOfNodes.length) && (
                            <Typography ref={secondLastTableRef} style={{color : 'white',marginTop : 20}}>شامل نود های دست نیافتی است که آن ها را حذف میکنیم</Typography>
                        )
                    }
                    {
                        (0<reachableNodesToWorkWith.length) && (reachableNodesToWorkWith.length === listOfNodes.length) && (
                            <Typography ref={secondLastTableRef} style={{color : 'white',marginTop : 20}}>شامل نود های دست نیافتی نیست پس نیازی به حذف هیچ نودی نیست</Typography>
                        )
                    }
                    {
                        (0<reachableNodesToWorkWith.length) && (reachableNodesToWorkWith.length < listOfNodes.length) && (
                            <TableContainer component={Paper} style={{marginTop : 20,minWidth : 300}}>
                                <Table aria-label="simple table" className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">نود</TableCell>
                                            <TableCell align="center">۰</TableCell>
                                            <TableCell align="center">۱</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {
                                            listOfNodes.map((item,index)=>{
                                                const reachable = reachableNodesToWorkWith.indexOf(item) > -1;
                                                return <TableRow key={index.toString()} style={{backgroundColor : reachable ? '' : 'pink'}}>
                                                    <TableCell align="center">
                                                        <Button onClick={()=>{}} style={{borderRadius : 35,width : 70,height : 70,border : finalStates[item[0]] ? '1px solid purple':undefined}}>
                                                            <Box style={{width : 50,height :50,borderRadius : 25,border : finalStates[item[0]] ? '1px solid purple':undefined}} display='flex' alignItems='center' justifyContent='center'>
                                                                {reachable ? (
                                                                    <Typography>{String.fromCharCode(item)}</Typography>
                                                                ):(
                                                                    <Typography style={{color : 'red'}}><del>{String.fromCharCode(item)}</del></Typography>
                                                                )}
                                                            </Box>
                                                        </Button>
                                                    </TableCell>
                                                    <TableCell align="center">
                                                        {reachable ? (
                                                            <Typography>{String.fromCharCode(selectedNodes[item].zero)}</Typography>
                                                        ):(
                                                            <Typography style={{color : 'red'}}><del>{String.fromCharCode(selectedNodes[item].zero)}</del></Typography>
                                                        )}
                                                    </TableCell>
                                                    <TableCell align="center">
                                                        {reachable ? (
                                                            <Typography>{String.fromCharCode(selectedNodes[item].one)}</Typography>
                                                        ):(
                                                            <Typography style={{color : 'red'}}><del>{String.fromCharCode(selectedNodes[item].one)}</del></Typography>
                                                        )}
                                                    </TableCell>
                                                </TableRow>
                                            })
                                        }
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        )
                    }
                    {
                        equivalencySteps.map((item,index)=>{
                            return <Typography key={index.toString()} style={{color : 'white',marginTop : index === 0 ? 50 : 10,fontSize : 15,fontWeight : 'bold'}}>{item} = {index} هم سنگی </Typography>
                        })
                    }
                    {
                        equivalencySteps.length > 0 && (
                            <TableContainer ref={lastTableRef} component={Paper} style={{marginTop : 50,minWidth : 300}}>
                                    <Table aria-label="simple table" className={classes.table}>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell align="center">نود</TableCell>
                                                <TableCell align="center">۰</TableCell>
                                                <TableCell align="center">۱</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {
                                                secondArray.map((item,index)=>{
                                                    return <TableRow key={index.toString()}>
                                                        <TableCell align="center">
                                                            <Button onClick={()=>{}} style={{borderRadius : 35,width : 70,height : 70,border : checkIfIsFinalState(item) ? '1px solid purple':undefined}}>
                                                                <Box style={{width : 50,height :50,borderRadius : 25,border : finalStates[item[0]] ? '1px solid purple':undefined}} display='flex' alignItems='center' justifyContent='center'>
                                                                    <Typography>{printArrayContainingAsciis(item)}</Typography>
                                                                </Box>
                                                            </Button>
                                                        </TableCell>
                                                        <TableCell align="center">
                                                            <Typography>{printArrayContainingAsciis(getArrayContaining(selectedNodes[item[0]].zero))}</Typography>
                                                        </TableCell>
                                                        <TableCell align="center">
                                                            <Typography>{printArrayContainingAsciis(getArrayContaining(selectedNodes[item[0]].one))}</Typography>
                                                        </TableCell>
                                                    </TableRow>
                                                })
                                            }
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                        )
                    }
                </Box>
            </Box>
        </div>
    );
}

export default App;
